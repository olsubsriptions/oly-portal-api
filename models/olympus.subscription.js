const db = require('../config/firebase').db
const crypto = require('../models/crypto')

async function getUserById (loginData) {
  const domain = crypto.hash(loginData.domain.toLowerCase())
  const userRef = db.collection('g6730').where('g6733', '==', domain)
  const querySnapshot = await userRef.get()
  if (!querySnapshot.empty) {
    // return querySnapshot.docs[0].id
    const userDbData = querySnapshot.docs[0].data()
    const susId = userDbData.b4321
    const susRef = db.collection('b4320').doc(susId)
    const document = await susRef.get()
    if (document.exists) {
      const susDbData = document.data()
      const securityKey = susDbData.b4322
      const username = await crypto.encrypt(loginData.username.toLowerCase() + '@' + loginData.domain.toLowerCase(), securityKey)
      const userRef2 = db.collection('g6730').where('g6732', '==', username)
      const querySnapshot2 = await userRef2.get()
      if (!querySnapshot2.empty) {
        return querySnapshot2.docs[0].id
      }
    }
  }
  return null
}

async function createSubscription (uidU, uidS, uidO, loginData, companyData, billingData) {
  const securityKey = await crypto.getKey()
  const derivativeKey = await crypto.getKey()
  const indexkey = await crypto.hash(uidS)
  console.log(securityKey)
  const username = await crypto.encrypt(loginData.username.toLowerCase() + '@' + loginData.domain.toLowerCase(), securityKey)
  const domain = crypto.hash(loginData.domain.toLowerCase()) // *
  const countryPrefix = loginData.country.code
  const primaryContactPhone = await crypto.encrypt(loginData.country.prefix + loginData.phoneNumber, securityKey)
  const primaryContactEmail = await crypto.encrypt(loginData.email, securityKey)

  const organizationName = await crypto.encrypt(companyData.company, securityKey)
  const taxID = await crypto.encrypt(companyData.document, securityKey)
  const state = companyData.state
  const province = companyData.province
  const city = companyData.city
  const address1 = await crypto.encrypt(companyData.address1, securityKey)
  const address2 = await crypto.encrypt(companyData.address2, securityKey)

  const administrativeContact = await crypto.encrypt(billingData.name, securityKey)
  const administrativeEmail = await crypto.encrypt(billingData.email, securityKey)
  const billingContact = await crypto.encrypt(billingData.name2, securityKey)
  const billingEmail = await crypto.encrypt(billingData.email2, securityKey)

  // const domainRef = db.collection('d2407').doc(domain)
  // const wrD = await domainRef.create({
  //   registerDate: new Date()
  // })
  // console.log(`dmns written at: ${wrD.writeTime.toDate()}`)

  const userRef = db.collection('g6730').doc(uidU)
  const wrU = await userRef.create({
    g6732: username,
    g6733: domain,
    b4321: uidS
  })
  console.log(`g6730 written at: ${wrU.writeTime.toDate()}`)

  const subscriptionRef = db.collection('b4320').doc(uidS)
  const wrS = await subscriptionRef.create({
    b4322: securityKey,
    b4323: true,
    b4324: derivativeKey,
    b4325: indexkey,
    f4461: uidO
  })
  console.log(`b4320 written at: ${wrS.writeTime.toDate()}`)

  const sOrganizationRef = db.collection('f4460').doc(uidO)
  const wrO = await sOrganizationRef.create({
    f4462: organizationName,
    f4463: address1,
    f4464: address2,
    f4465: city,
    f4466: province,
    f4467: state,
    f4468: countryPrefix,
    f4469: primaryContactPhone,
    f4470: taxID,
    f4471: primaryContactEmail,
    f4472: administrativeContact,
    f4473: administrativeEmail,
    f4474: billingContact,
    f4475: billingEmail,
    f4476: true
  })
  console.log(`f4460 written at: ${wrO.writeTime.toDate()}`)
}

module.exports = {
  createSubscription,
  getUserById
}

const db = require('../config/firebase').db
const crypto = require('../models/crypto')

async function organizationExist (loginData) {
  const domain = crypto.hash(loginData.domain.toLowerCase())
  const userRef = db.collection('g6730').where('g6733', '==', domain)
  const querySnapshot = await userRef.get()
  return (querySnapshot && !querySnapshot.empty)
}

module.exports = {
  organizationExist
}

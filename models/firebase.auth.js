const admin = require('../config/firebase').admin

async function generateEmailVerificationLink (userFirebaseEmail) {
  const url = 'https://subs-001.web.app';
  // const url = process.env.MAIL_REDIRECT;
  const link = await admin.auth().generateEmailVerificationLink(userFirebaseEmail, {
    url: url
  })
  
  console.log(`Firebase EmailVerificationLink created: ${link}`)
  return link
}

async function generatePasswordResetLink (userFirebaseEmail) {
  const link = await admin.auth().generatePasswordResetLink(userFirebaseEmail, {
    url: process.env.MAIL_REDIRECT
  })
  console.log(`Firebase generatePasswordResetLink created: ${link}`)
  return link
}

async function phoneRegistered (loginData) {
  let phone = null
  if (loginData && loginData['phoneNumber'] && loginData['country'] && loginData['country']['prefix']) {
    phone = loginData['country']['prefix'] + loginData['phoneNumber']
  } else {
    return false
  }

  try {
    await admin.auth().getUserByPhoneNumber(phone)
    return true
  } catch (e) {
    return !(e.code === 'auth/user-not-found')
  }
}

async function createFireBaseUser (uid, loginData) {
  let phone = null
  if (loginData && loginData['phoneNumber'] && loginData['country'] && loginData['country']['prefix']) {
    phone = loginData['country']['prefix'] + loginData['phoneNumber']
  }

  console.log(phone)

  const userProperties = {
    uid: uid,
    email: uid + '@olympus-cloud.com',
    emailVerified: false,
    password: loginData['password'],
    displayName: (loginData['username'] || '').toLowerCase() + '@' + (loginData['domain'] || '').toLowerCase(),
    disabled: false,
    phoneNumber: phone
  }

  const record = await admin.auth().createUser(userProperties)
  console.log(`Firebase user created at: ${record.uid}`)
}

module.exports = {
  phoneRegistered,
  createFireBaseUser,
  generateEmailVerificationLink,
  generatePasswordResetLink
}

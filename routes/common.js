const express = require('express')
const { generatePasswordResetLink } = require('../models/firebase.auth')
const { getUserById } = require('../models/olympus.subscription')
const { phoneRegistered } = require('../models/firebase.auth')
const { organizationExist } = require('../models/olympus.organization')
const router = express.Router()

router.post('/validate/user', async function (req, res, next) {
  const loginData = req.body['login']
  if (!loginData ||
    !loginData.username ||
    !loginData.domain) {
    res.send({
      status: false,
      message: 'NO_LOGIN_DATA'
    })
    next()
    return
  }

  try {
    const userID = await getUserById(loginData)
    if (!userID) {
      res.send({
        status: false,
        message: 'USER_NOT_FOUND'
      })
      next()
      return
    }
    res.send({
      status: true,
      user: userID + '@olympus-cloud.com'
    })
  } catch (e) {
    console.warn(e.code || e.statusCode || e.message)
    res.send({
      status: false,
      message: e.code || e.statusCode || e.message
    })
  }
})

router.post('/validate/register', async function (req, res, next) {
  const loginData = req.body['login']
  if (!loginData ||
    !loginData.username ||
    !loginData.domain ||
    !loginData.password ||
    !loginData.email ||
    !loginData.country ||
    !loginData.phoneNumber
  ) {
    res.send({
      status: false,
      message: 'NO_LOGIN_DATA'
    })
    next()
    return
  }
  try {
    if (await organizationExist(loginData)) {
      res.send({
        status: false,
        message: 'INVALID_ORGANIZATION'
      })
      next()
      return
    }
    if (await phoneRegistered(loginData)) {
      res.send({
        status: false,
        message: 'PHONE_ALREADY_EXIST'
      })
      next()
      return
    }
    res.send({
      status: true
    })
  } catch (e) {
    console.warn(e.code || e.statusCode || e.message)
    res.send({
      status: false,
      message: e.code || e.statusCode || e.message
    })
  }
})

router.post('/reset/password/user', async function (req, res, next) {
  const loginData = req.body['login']
  if (!loginData ||
    !loginData.username ||
    !loginData.domain) {
    res.send({
      status: false,
      message: 'NO_LOGIN_DATA'
    })
    next()
    return
  }

  try {
    const userID = await getUserById(loginData)
    if (!userID) {
      res.send({
        status: false,
        message: 'USER_NOT_FOUND'
      })
      next()
      return
    }
    const link = await generatePasswordResetLink(userID + '@olympus-cloud.com')

    res.send({
      status: true,
      data: userID,
      link: link
    })
  } catch (e) {
    console.warn(e.code || e.statusCode || e.message)
    res.send({
      status: false,
      message: e.code || e.statusCode || e.message
    })
  }
})

module.exports = router

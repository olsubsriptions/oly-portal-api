const express = require('express')
const uuid = require('uuid/v4')
const { sendMail } = require('../config/mail')
const { organizationExist } = require('../models/olympus.organization')
const { createSubscription } = require('../models/olympus.subscription')
const { createFireBaseUser, phoneRegistered, generateEmailVerificationLink } = require('../models/firebase.auth')
const router = express.Router()

router.post('/', async function (req, res, next) {
  console.log(JSON.stringify(req.body))
  const loginData = req.body['login']
  const companyData = req.body['company'] || {}
  const billingData = req.body['billing'] || {}

  if (!loginData ||
    !loginData.username ||
    !loginData.domain ||
    !loginData.password ||
    !loginData.email ||
    !loginData.country ||
    !loginData.phoneNumber
  ) {
    res.send({
      status: false,
      message: 'NO_LOGIN_DATA'
    })
    next()
    return
  }

  if (!companyData ||
    !companyData.company ||
    !companyData.document ||
    !companyData.state ||
    !companyData.city ||
    !companyData.province ||
    !companyData.address1 ||
    !companyData.address2
  ) {
    res.send({
      status: false,
      message: 'NO_COMPANY_DATA'
    })
    next()
    return
  }

  try {
    if (await organizationExist(loginData)) {
      res.send({
        status: false,
        message: 'INVALID_ORGANIZATION'
      })
      next()
      return
    }
    if (await phoneRegistered(loginData)) {
      res.send({
        status: false,
        message: 'PHONE_ALREADY_EXIST'
      })
      next()
      return
    }
    const uidU = uuid()
    const uidS = uuid()
    const uidO = uuid()
    await createFireBaseUser(uidU, loginData)
    const link = await generateEmailVerificationLink(uidU + '@olympus-cloud.com')
    await createSubscription(uidU, uidS, uidO, loginData, companyData, billingData)
    sendMail(loginData, link)
    res.send({
      status: true
    })
  } catch (e) {
    console.error(e)
    console.warn(e.code || e.statusCode || e.message)
    res.send({
      status: false,
      message: e.code || e.statusCode || e.message
    })
  }
})

module.exports = router

const express = require('express')
const router = express.Router()
const countries = require('../data/countries')

router.get('/', async function (req, res, next) {
  try {
    res.send(countries)
  } catch (e) {
    console.warn(e.code || e.statusCode || e.message)
    res
      .status(500)
      .send({
        status: false,
        message: e.code || e.statusCode || e.message
      })
  }
})

module.exports = router

const admin = require('firebase-admin')
const serviceAccount = require('./key.json')

const defaultApp = admin.initializeApp({
  credential: admin.credential.cert(serviceAccount)
})

const db = admin.firestore()

module.exports = {
  admin, defaultApp, db
}

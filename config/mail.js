const mailJet = require('node-mailjet')
  .connect(
    process.env.MAIL_K,
    process.env.MAIL_S,
    {
      version: 'v3.1',
      perform_api_call: true
    }
  )

function sendMail (loginData, confirmationURL) {
  const displayName = (loginData['username'] || '').toLowerCase() + '@' + (loginData['domain'] || '').toLowerCase()
  const requestMail = mailJet
    .post('send')
    .request({
      Messages: [
        {
          From: {
            Email: process.env.MAIL_FROM,
            Name: process.env.MAIL_FROM_NAME
          },
          To: [
            {
              Email: loginData.email,
              Name: displayName
            }
          ],
          TemplateID: 910623,
          TemplateLanguage: true,
          Subject: 'Olympus-Cloud suscription!',
          Variables: {
            action_url: confirmationURL
          }
        }
      ]
    })
  // await request;
  requestMail
    // .then((result) => {
    //     return true;
    // })
    .catch((err) => {
      console.warn('WARN', err.statusCode)
    })
}

module.exports = {
  sendMail
}
